package pl.r4fall.sumgame.app;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import pl.r4fall.sumgame.app.utils.GameUtils;

import java.util.List;

public class SumGameActivity extends AppCompatActivity {

    private EditText[][] numbers;
    private TextView[][] sum;

    private Button submitButton;
    private Button continueButton;
    private Button newGameButton;
    private Button exitGameButton;

    private CountDownTimer timer;
    private TextView chronometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sum_game);

        numbers = new EditText[3][3];
        numbers[0][0] = (EditText) findViewById(R.id.number_00);
        numbers[0][1] = (EditText) findViewById(R.id.number_01);
        numbers[0][2] = (EditText) findViewById(R.id.number_02);
        numbers[1][0] = (EditText) findViewById(R.id.number_10);
        numbers[1][1] = (EditText) findViewById(R.id.number_11);
        numbers[1][2] = (EditText) findViewById(R.id.number_12);
        numbers[2][0] = (EditText) findViewById(R.id.number_20);
        numbers[2][1] = (EditText) findViewById(R.id.number_21);
        numbers[2][2] = (EditText) findViewById(R.id.number_22);

        sum = new TextView[2][3];
        sum[0][0] = (TextView) findViewById(R.id.sumh_0);
        sum[0][1] = (TextView) findViewById(R.id.sumh_1);
        sum[0][2] = (TextView) findViewById(R.id.sumh_2);
        sum[1][0] = (TextView) findViewById(R.id.sumv_0);
        sum[1][1] = (TextView) findViewById(R.id.sumv_1);
        sum[1][2] = (TextView) findViewById(R.id.sumv_2);

        submitButton = (Button) findViewById(R.id.submit_button);
        continueButton = (Button) findViewById(R.id.continue_button);
        newGameButton = (Button) findViewById(R.id.new_game_button);
        exitGameButton = (Button) findViewById(R.id.exit_game_button);

        continueButton.setEnabled(false);
        newGameButton.setEnabled(false);

        fillSum();

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitButton.setEnabled(false);
                if (isValidNumbers()) {
                    newGameButton.setEnabled(true);
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();
                } else {
                    continueButton.setEnabled(true);
                    newGameButton.setEnabled(true);
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.invalid), Toast.LENGTH_SHORT).show();
                }
            }
        });

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitButton.setEnabled(true);
                continueButton.setEnabled(false);
                newGameButton.setEnabled(false);
            }
        });

        newGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitButton.setEnabled(true);
                continueButton.setEnabled(false);
                newGameButton.setEnabled(false);

                fillSum();
            }
        });

        exitGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });

        chronometer = (TextView) findViewById(R.id.chronometer);
    }

    private void fillSum() {
        List<Integer> permutation = GameUtils.getRandomDigits();

        for (int j = 0; j < 3; j++) {
            sum[0][j].setText(String.valueOf(
                    permutation.get(j * 3) +
                    permutation.get(j * 3 + 1) +
                    permutation.get(j * 3 + 2)
            ));
        }

        for (int j = 0; j < 3; j++) {
            sum[1][j].setText(String.valueOf(
                    permutation.get(j) +
                    permutation.get(j + 3) +
                    permutation.get(j + 6)
            ));
        }

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                numbers[i][j].setText("");
            }
        }

        if (timer != null)
            timer.cancel();

        timer = new CountDownTimer(120000, 1000) {

            public void onTick(long millisUntilFinished) {
                long minutes = millisUntilFinished / 60000;
                long secs = (millisUntilFinished % 60000) / 1000;
                chronometer.setText((minutes > 9 ? minutes : ("0" + minutes)) + ":" + (secs > 9 ? secs : ("0" + secs)));
            }

            public void onFinish() {
                chronometer.setText("00:00");
                newGameButton.setEnabled(true);
                if (isValidNumbers()) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();
                } else {
                    submitButton.setEnabled(false);
                    continueButton.setEnabled(false);
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.game_over), Toast.LENGTH_SHORT).show();
                }
            }


        }.start();
    }

    private boolean isValidNumbers() {
        for (int j = 0; j < 3; j++) {
            if (GameUtils.getIntFromTextView(sum[0][j]) !=
                    GameUtils.getIntFromEditText(numbers[j][0]) +
                    GameUtils.getIntFromEditText(numbers[j][1]) +
                    GameUtils.getIntFromEditText(numbers[j][2]))
                return false;
        }

        for (int j = 0; j < 3; j++) {
            if (GameUtils.getIntFromTextView(sum[1][j]) !=
                    GameUtils.getIntFromEditText(numbers[0][j]) +
                    GameUtils.getIntFromEditText(numbers[1][j]) +
                    GameUtils.getIntFromEditText(numbers[2][j]))
                return false;
        }

        return true;
    }
}
