package pl.r4fall.sumgame.app.utils;

import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameUtils {

    public static List<Integer> getRandomDigits() {
        Random random = new Random();
        List<Integer> permutation = new ArrayList<Integer>();

        while (permutation.size() != 9) {
            Integer number = random.nextInt(10);
            if (!permutation.contains(number) && !number.equals(0))
                permutation.add(number);
        }

        return permutation;
    }

    public static int getIntFromTextView(TextView textView) {
        return getIntFromString(textView.getText().toString());
    }

    public static int getIntFromEditText(EditText editText) {
        return getIntFromString(editText.getText().toString());
    }

    private static int getIntFromString(String s) {
        if (s.length() == 0)
            return 0;

        return Integer.valueOf(s);
    }

}
